# Framework of Angry Illegalists

Like 'Steal This Wiki', but extreme, radical and angry. GRRRRR

## WHAT IS F.A.I.

FAI was initially a satire project of Uproot Collective, but we realized that smashing the state takes more than a joke. So we make it real.

## Index

* [Anarchy In Action](/a)
* [Bike Maintenance](/b)
* [Carjacking & Carhopping](/c)
* [Dumpster Diving](/d)
* [Explosive](/e)
* [First Aids](/f)
* [Greensthumbing, Guerilla & Gunsmithing](/g)
* [Hacking](/h)
* [Instigation](/i)
* [Jamming Cop's Freqs](/j)
* [Knitting, Kali & Knuckles Of Brass](/k)
* [Lying Like A Pro](/l)
* [Mushrooming](/m)
* [Neutralizing The Class Traitors](/n)
* [Omitting Forensic Evidences](/o)
* [Praxis](/p)
* [Queer As In Bashing Phobic](/q)
* [Radio Of Pirates](/r)
* [Shoplifting](/s)
* [Tinkering](/t)
* [Undercovers Spotting](/u)
* [Vaulting & Parkour](/v)
* [Wheatpasting](/w)
* [Xtermination of Whiteness](/x)
* [Y For Everything](/y)
* [Ziplining & Climbing](/z)